# Canopy Ansible Role

This role installs and configures the [Canopy](https://gitlab.com/mergetb/tech/canopy)
virtual network service or client.

## Variables

| name | required | default | description |
| ---- | ---------| ------- | ----------- |
| server | no | no | install canopy server |
| client | no | no | install canopy client |
| endpoint | no | 0.0.0.0:6074 | endpoint for canopy server to listen on |
| arch | no | amd64 | binary instruction set architecture |

### Available architectures

- arm5
- arm7
- amd64

## Examples

### Canopy Server

```yaml
- include_role:
    name: canopy
  vars:
    server: yes
    arch: arm5
```

```yaml
- include_role:
    name: canopy
  vars:
    server: yes
    endpoint: 10.47.3.5:6799
```

### Canopy Client

```yaml
- include_role:
    name: canopy
  vars:
    client: yes
    arch: amd64
```

